import sqlite3

conexion = sqlite3.connect('database.db')

with open('script_db.sql') as f:
    conexion.executescript(f.read())
            
conexion.commit()
conexion.close()
