# Proyecto Final Tecnología y Servicios de Internet
## Ery Jose Polanco Ciprian      18-EISN-2-031

Página de Consultas desarrollada con HTML5, Python y SQLite3

## Para la Instalación del Servidor
1 - Instalar Python (Si ya lo tiene instalado omitir este paso).
2 - Ejecutar el archivo llamado "Librerías.cmd".
3 - Listo.
4 - Opcional: Crear un acceso directo en el escritorio del archivo "Start.cmd"

El instalador de Python está en la carpeta principal. Cuando realice la instalación
puede borrar el instalador de Python y el archivo "Librerías.cmd" y quedarse solo con la carpeta.

## Para Iniciar el Servidor
1 - Ejecuta el archivo Start.cmd
2 - No cierre la ventana de la consola mientras use la App

## Para Cerrar la App
1 - Cierre el Navegador
2 - Ya puede cerrar la ventana de la consola

## Si cierra la ventana de la consola, no podrá utilizar la app, ya que está deteniendo el servidor.
