import sqlite3; import datetime
from flask import Flask, render_template, request, url_for, flash, redirect
from werkzeug.exceptions import abort

"""
Para fecha y hora
datetime.datetime.now()
"""

def conectar_db():
    conexion = sqlite3.connect('database.db')
    conexion.row_factory = sqlite3.Row
    return conexion




app = Flask(__name__)

@app.route("/")
def index():
    return render_template('index.html')


@app.route("/gestion")
def gestion():
    conexion = conectar_db()
    consult = "SELECT consulta, nombre, apellido, descripcion, monto, fecha FROM consulta;"
    consultas = conexion.execute(consult).fetchall()
    return render_template('gestion.html', consultas = consultas)


@app.route("/registro", methods=('GET','POST'))
def registro():
    if request.method == 'POST':
        nombre = request.form['nombre']
        apellido = request.form['apellido']
        descripcion = request.form['descripcion']
        monto = request.form['monto']
        fecha = datetime.datetime.now()

        conn = conectar_db()
        
        conn.execute('INSERT INTO consulta (nombre, apellido, descripcion, monto, fecha) VALUES (?,?,?,?,?)',
                     (nombre, apellido, descripcion, monto, fecha))
        conn.commit()
        conn.close()
        return redirect(url_for('registro'))

    return render_template('registro.html')

@app.route("/dashboard", methods=('GET','POST'))
def dashboard():
    conn = conectar_db()
    pagos = conn.execute("SELECT monto FROM consulta").fetchall()
    monto = 0
    monto_P = 0
    
    for i in pagos:
        for e in i:
            monto += e
    if request.method == "POST":


        porciento = int(request.form['porciento'])

        porciento /= 100

        monto_P = monto * porciento

        conn.commit()
        conn.close()

    return render_template("dashboard.html", monto = monto, monto_P = monto_P)


if __name__ == '__main__':
    app.run(port = 3000, debug = True)
